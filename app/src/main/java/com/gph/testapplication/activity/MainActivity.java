package com.gph.testapplication.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.gph.testapplication.BuildConfig;
import com.gph.testapplication.R;
import com.gph.testapplication.service.TimerService;

/**
 * Created by asus on 09-Nov-18.
 */

public class MainActivity extends AppCompatActivity {

    Intent intent;
    //Different Id's will show up as different notifications
    private int mNotificationId = 1;

    //Some things we only have to set the first time.
    private boolean firstTime = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView = findViewById(R.id.txt_name);

        if (BuildConfig.BUILD_VARIANT.equalsIgnoreCase("production")) {
            textView.setText("production");
        } else if (BuildConfig.BUILD_VARIANT.equalsIgnoreCase("QA")) {
            textView.setText("QA");
        }

        intent = new Intent(MainActivity.this, TimerService.class);
        startService(intent);
        registerReceiver(broadcastReceiver, new IntentFilter(TimerService.BROADCAST_ACTION));

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            int time = intent.getIntExtra("time", 0);

            Log.e("MainActivity", "Time " + time);

            int mins = time / 60;
            int secs = time % 60;
            String str = "" + mins + ":" + String.format("%02d", secs);
            updateNotification(str);
        }
    };


    private void updateNotification(String message) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (firstTime) {

            mBuilder.setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentTitle("My Notification")
                    .setOnlyAlertOnce(true);
            firstTime = false;
        }
        mBuilder.setContentText(message);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel("my_channel",
                    "gph_notification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
        }
        mNotificationManager.notify(mNotificationId, mBuilder.build());
    }

}
